
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from Helicoidal import helicoidal

"""
NOTA 
4.5

- El código no funciona en los limites por ejemplo en B=0, o decirle al usuario que 
valores poner

-SE RECOMIENDA PONER ESTA PARTE DEL CÓDIO DENTRO DE LA CLASE
TH1 = H1.period() #llamo al metodo period (periodo de un giro)
t = np.linspace(0,n*TH1,1000) #tiempo de vuelo en array (veo n vueltas)
H1.t = t #le asigno el tiempo de vuelo al objeto H1 (modifico el tiempo de vuelo para poder ver la trayectoria completa)

"""

if __name__=="__main__":
    
    #definiendo parametros iniciales
    E = 13.8 * (1.602)*10**(-19) #Energía de la particula en J
    m = 1.67*10**(-27) #masa de la particula en kg (proton)
    q = -1.602*10**(-19) #carga de la particula en C (proton)
    B = 0#0.5 #campo magnético en T
    ang = 30 #angulo de entrada en grados
    n = 10 #numero de giros que quiero ver


    H1 = helicoidal(0.1,E,m,q,B,ang) #heredo la clase helicoidal a la helicoidal1 (H1)
    TH1 = H1.period() #llamo al metodo period (periodo de un giro)
    t = np.linspace(0,n*TH1,1000) #tiempo de vuelo en array (veo n vueltas)
    H1.t = t #le asigno el tiempo de vuelo al objeto H1 (modifico el tiempo de vuelo para poder ver la trayectoria completa)
    print("El periodo de la trayectoria es: {} s".format(TH1))
    print("La velocidad de la particula es: {} m/s".format(H1.v))
    print("El radio de la trayectoria es: {} m".format(H1.R))
    print("La frecuencia angular es: {} rad/s".format(H1.w))
    H1.motiongraf() #llamo al metodo motiongraf

  




