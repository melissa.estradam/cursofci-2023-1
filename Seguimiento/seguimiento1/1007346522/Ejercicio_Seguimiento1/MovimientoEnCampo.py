import numpy as np
import matplotlib.pyplot as plt

class MovimientoEnCampoMagnetico():

    def __init__(self,R_0,Ek,theta,phi,B, m = 9.11e-31,q = -1.6e-19):
        print('Inicializando Movimiento en Campo Magnético Constante en dirección +z')

        # Definiendo Atributos de Clase
        self.R_0 = R_0
        
        if B == 0 or q == 0 or q == 0:
            print('Valores no son válidos, se toman valores por defecto')
            self.B = 1e-6
            self.m = 1e-31
            self.q = 1e-19
        else:
            self.B = B*1e-6 # Conversión a tesla
            self.m = m
            self.q = q

        self.Ek = Ek*1.6e-19 # Conversión a julios
        self.theta = np.deg2rad(theta)
        self.phi = np.deg2rad(phi)

        # Calculando a partir de la Energía Cinética y los ángulos direccionales
        # Las componentes paralela y perpendicular al campo
        self.V_0 = np.sqrt(2*self.Ek/self.m)
        self.V_paral = self.V_0*np.round(np.cos(self.theta),3)
        self.V_perp  = self.V_0*np.round(np.sin(self.theta),3)

        # Calculando el radio de la elise y la velocidad angular 
        self.rho = self.m*abs(self.V_perp)/(abs(self.q)*self.B)
        self.Omega = abs(self.q)*self.B/self.m
    

    # Posicion con respecto al tiempo en cada componente
    def PosX(self,t):
        return self.R_0[0] + self.rho*np.round(np.sin(self.phi)+np.sin(self.Omega*t-self.phi),2)
        
    def PosY(self,t):
        return self.R_0[1] + self.rho*np.round(-np.cos(self.phi)+np.cos(self.Omega*t-self.phi),2)
    
    def PosZ(self,t):
        return self.R_0[2] + self.V_paral*t

    # Datos de la trayectoria y sus características
    def info(self):

        CondIni = [*self.R_0,self.Ek/1.6e-19,round(np.rad2deg(self.theta),2),round(np.rad2deg(self.phi),2),self.B*1e6,self.m,self.q]
        Carac = [self.V_paral,self.V_perp,self.rho,self.Omega]
        Text1 = 'x_0 = {} \ny_0 = {} \nz_0 = {} \nE_k = {} \nTheta = {} \nPhi = {} \nB = {} \nm = {} \nq = {}'.format(*CondIni)
        Text2 = 'V_paralela = {} \nV_perpendicular = {} \nrho = {} \nOmega = {} \n'.format(*Carac)
        Cabecera = '-'*10+ 'Condiciones iniciales:' +'-'*10
        Cabecera2 = '-'*10+ 'Características:' +'-'*10

        return Cabecera+'\n'+ Text1+'\n'+Cabecera2+'\n'+ Text2
    

    def Graf(self,Nmax = 10000):

        t_array = np.linspace(0,100/self.Omega,Nmax) # Arreglo de tiempo

        R_f = [self.PosX(t_array[-1]),self.PosY(t_array[-1]),self.PosZ(t_array[-1])] # Posición final

        ax = plt.figure(figsize=(7,7)).add_subplot(projection='3d')
        
        ax.plot(self.PosX(t_array),self.PosY(t_array),self.PosZ(t_array),label='Trayectoria ',linewidth= 0.5,c='sienna')
        ax.scatter(*self.R_0,label= 'Inicio = ({},{},{})'.format(*self.R_0),c='darkred') # Punto inicial
        ax.scatter(*R_f,label='Final= ({},{},{})'.format(*np.round(R_f,2)),c='g')        # Punto final

        ax.set_xlabel('Posición X')
        ax.set_ylabel('Posición Y')
        ax.set_zlabel('Posición Z')
        ax.grid()
        ax.legend()
        plt.savefig('Plot_Movimiento_Campo.png')