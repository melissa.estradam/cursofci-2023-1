import numpy as np
import matplotlib.pyplot as plt
from integral_montecarlo import Integral
from integral_montecarlo import MonteCarlo

if __name__=="__main__":
    
    N=100000            # número de puntos
    a=0             # limite inferior
    b=np.pi             # límite superior
    x0=np.pi/2      # cero de la función a integrar
    f = lambda x: x**2*np.cos(x)        # función a integrar

    Int = Integral(f,a,b)
    print(f"Resultado con scipy {np.round(Int.Analitica(),2)}")

    Int1=MonteCarlo(f,a,x0,int(N/2))
    Int2=MonteCarlo(f,x0,b,int(N/2))

    print(f"Resultado Montecarlo {np.round(Int1.Calculo()+Int2.Calculo(),2)}")
    
    int_mc=[]           # arreglo para almacenar los valores de la integral con montecarlo

    NN=np.arange(10,N,1000)
    for j in range(len(NN)):
        Int1=MonteCarlo(f,a,x0,int(NN[j]))
        Int2=MonteCarlo(f,x0,b,int(NN[j]))
        int_mc.append(Int1.Calculo()+Int2.Calculo())
    
    plt.title("Comparación integral por Montecarlo y por scipy")
    plt.plot(NN,int_mc,label="Montecarlo")
    plt.plot(NN,np.ones(len(NN))*Int.Analitica(),label="Scipy")
    plt.legend()
    plt.grid()
    plt.savefig("Comparacion.png")
    plt.show()