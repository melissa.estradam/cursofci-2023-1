from decaimiento import Decaimiento
import numpy as np

if __name__=="__main__":
    
#----------- PARAMETROS --------------
    dt=0.1      # paso de tiempo
    T_f=100      # tiempo final
    λ_Ra=np.log(2)/14.8     # tiempo de vida medio del Radio 255
    λ_Ac=np.log(2)/10.8        # tiempo de vida medio del Actinio 255
    N_Ra=1000      # número inicial de átomos del Radio 255
    N_Ac=0      # número inicial de átomos del Actinio 255



    numero=Decaimiento(dt,N_Ra,N_Ac,T_f,λ_Ra,λ_Ac)
    numero.Plot()
