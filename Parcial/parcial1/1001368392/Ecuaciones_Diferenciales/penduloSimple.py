import numpy as np
from sympy import Function, dsolve, Derivative, lambdify, symbols
import matplotlib.pyplot as plt
from sympy.abc import x, y 
class penduloSimple():
    """
    Esta clase resuelve un pendulo simple de manera analitica y numerica. Tambien tiene metodos para
    graficar las soluciones.
    
    Los parametros que deben ingresarse a la clase son:
    - Valor del angulo inicial: th0 (Radianes)
    - Valor de la velocidad angular inicial: dth0 (Radianes/s)
    - Relacion gravedad/longitud de la cuerda: gl (Real adimensional)
    - Tiempo inicial: ti (segundos)
    - Tiempo final: tf (segundos)
    - Numero de iteraciones en el metodo de Euler: n (Entero positivo)
    """
    def __init__(self, th0, dth0, gl, ti, tf, n):
        self.th0 = th0 
        self.dth0 = dth0 
        self.gl = int(gl) 
        self.ti = ti
        self.tf = tf
        self.n = n
    
    # Solucion exacta para el angulo del pendulo simple:
    def solExacta(self):
        t = symbols('t')
        th = Function('th')
        eq = Derivative(th(t), t, t) + self.gl * th(t) 
        cond0 = {th(self.ti): self.th0, th(t).diff(t).subs(t, self.ti): self.dth0}
        sol = dsolve(eq, th(t), ics= cond0)
        return sol
    
    # Salto en t del metodo de Euler:
    def h(self):
        if self.tf <= self.ti:
            raise Exception("Ingrese un valor final mayor al inicial")
        return (self.tf - self.ti)/self.n
    
    # Arreglo de tiempos dados los puntos finales, iniciales, h y el numero de iteraciones
    def arrt(self):
        if self.tf <= self.ti:
            raise Exception("Ingrese un valor final mayor al inicial")
        h = self.h()
        arrt = np.linspace(self.ti, self.tf + h, self.n)
        return arrt
    
    # Metodo de Euler para la integracion de la ecuacion en el intervalo dado (Se usa el desacople)
    def inteuler(self):
        h = self.h()
        tarr = self.arrt()
        tharr = np.array([self.th0])
        dtharr = np.array([self.dth0])
        func = lambda s0, s1 : np.array([s1, - self.gl * s0])
        for i in tarr[:-1]:
            thtemp = tharr[-1] + h * func(tharr[-1],dtharr[-1])[0]
            dthtemp = dtharr[-1] + h * func(tharr[-1],dtharr[-1])[1]
            tharr = np.append(tharr, thtemp)
            dtharr = np.append(dtharr, dthtemp)
        return tharr
    
    # Grafica de las soluciones analitica y numerica para su comparacion
    def comparesols(self):
        t = symbols('t')
        th = Function('th')
        excsol = lambdify(t, self.solExacta().rhs)
        arrt = self.arrt()
        theul = self.inteuler()
        thexc = excsol(arrt)
        plt.figure(figsize=(10,8))
        plt.plot(arrt, theul , label="Solucion integrada por metodo de Euler",color="red")
        plt.plot(arrt, thexc , label="Solucion analitica",color="blue")
        plt.grid()
        plt.xlabel("t (s)")
        plt.ylabel("theta (radianes)")
        plt.legend()
        plt.savefig("comparacion_pend.png")
