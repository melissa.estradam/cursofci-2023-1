
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


class TiroParabolico():
    #constructor
    def __init__(self,xi,h,v0,ang,t):

        #Se definen los Atributos a utilizar
        self.xi=xi
        self.h=h
        self.v0=v0
        self.ang=ang
        self.t=t
        self.g=-9.8 #aceleración gravitacional en la tierra
        self.v0x=v0*np.cos(ang*np.pi/180)
        self.v0y=v0*np.sin(ang*np.pi/180)

# Método para calcular la posicion en y
    def viewposx(self):
        px=self.xi + self.v0x*self.t
        return px

    # Método para calcular la posicion en y
    def viewposy(self):
        py=self.h + self.v0y*self.t+0.5*self.g*self.t**2
        return py
    
    # Método para calcular la velocidad en x en un tiempo t
    def vx(self, t):
        vx = self.v0x* t
        return vx

    # Método para calcular la velocidad en el eje y en un tiempo t
    def vy(self, t):
        vy = self.v0y + self.g * t
        return vy
    
    # Método para calcular la distancia maxima del objeto en x sin friccion
    def xmax1(self):
        xmax1 = 2*self.v0x*self.v0y/self.g
        return xmax1
    
    # Método para calcular la altura maxima del objeto
    def ymax(self):
        ymax = - self.v0y ** 2 / (2 * self.g)
        return ymax
    
    # Método para calcular el tiempo de vuelo
    def t_vuelo(self):
        t_vuelo = -2 * self.v0y / self.g
        return t_vuelo
    
#Herencia de clase añadiendo termino de friccion
class TiroParabolicoFcc(TiroParabolico):

    def __init__(self, xi, h, v0, ang,t,afr):
        super().__init__(xi, h, v0, ang, t)
        self.afr=afr

    #metodo para incluir el termino de friccion
    def vfrx(self):
        px=self.xi + self.v0x*self.t-0.5*self.afr*self.t**2
        return px
    
    # Método para el alcanze máximo del objeto en el eje x cuando hay friccion
    def xmax(self):
        xmax = -(2 * self.v0y * self.v0x / self.g + (1 / 2) * self.afr * (2 * self.v0y / self.g) ** 2)
        return xmax
    
class t_parab_fricc_y(TiroParabolicoFcc): #Se añaden corrientes de vientos en Y

    #metodo constructor para hallar resultados con friccion en y
    def __init__(self, xi, h, v0, ang, t, afr, vien_y):
        super().__init__(xi, h, v0, ang, t, afr)
        self.vien_y=vien_y

    #metodo para incluir el termino de friccion en y
    def vfry(self): #pos en y con fricccion del viento en y
        py=self.h + self.v0y*self.t+0.5*(self.vien_y+self.g)*self.t**2
        return py

    #polimorfismo del tiempo de vuelo
    def t_vuelo(self):
        t_vuelo = -2*self.v0y/(self.g+self.vien_y)
        return t_vuelo
    
    