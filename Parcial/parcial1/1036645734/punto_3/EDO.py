import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class PenduloSimple():

    def __init__(self, a,b,n,ang_ini,f,wi, point=False):
        print("inicilizando solucion numerica")
        self.a=a
        self.b=b
        self.n=n
        self.ang_ini=ang_ini
        self.f=f
        self.wi=wi
        self.theta=[]
        self.omega=[]


    def h(self): #Defino los pasos
        return (self.b-self.a)/self.n 


    """def X(self): #funcion para x dado un punto limite o no
        
        if self.point:
            self.b=self.point
            self.theta=np.arange(self.a,self.point,self.h())
        else:
            self.theta=np.arange(self.a,self.b,self.h())"""


    def Euler(self): #defino la funcion de euler que depende de theta e omega
        self.theta=[self.ang_ini]

        self.omega=[self.wi] #primer valor para wi, es dado
        for i in range(self.n-1):
            self.omega.append(self.omega[i]+self.h()*(self.f(self.theta[i],self.omega[i])))
            self.theta.append(self.theta[i]+self.omega[i+1]*self.h())
        return self.theta, self.omega
    
    def grafica(self):
        theta,omega=self.Euler()
        t = np.arange(self.a,self.b,self.h()) 

        sol_analitica=lambda t: np.cos(4*t)
        theta_analitica=sol_analitica(t)

        #se procede a graficar la solucion analitica y numerica
        plt.plot(t,theta, label='solucion numerica')
        plt.plot(t,theta_analitica, label='solucion analitica')
        plt.ylabel("theta")
        plt.xlabel("t")
        plt.grid()
        plt.legend()
        plt.title("comparacion numerica y analitica")
        plt.show()
        plt.savefig("pendulo_simple.png")