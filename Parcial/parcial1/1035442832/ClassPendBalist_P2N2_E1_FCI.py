
# Importo librerías a usar.
import numpy as np
import matplotlib.pyplot as plt

# Se supone un choque inelástico, i.e. la bala y el péndulo quedan pegados.
class PenduloBalistico(): # Definición de clase.

    def __init__(self, mb, mp, l, vp): # Método constructor.
        print('Inicializando clase PenduloBalistico.')
        
        self.mb = mb # Masa de la bala.
        self.mp = mp # Masa del péndulo.
        self.l = l # Longitud del péndulo.
        self.vp = vp # Velocidad del péndulo tras colisión.
        self.g = 9.81 # Aceleración de la gravedad.

    def DesviacionAngulo(self): # Desviación del ángulo tras impacto.
        theta = np.arccos(1 - ((self.vp)**2/(2*self.g*self.l)))
        return np.round(np.rad2deg(theta),4)

    def VelocidadBala(self): # Velocidad de bala antes de impacto.
        theta = np.radians(self.DesviacionAngulo()) # Para que dependa del ángulo.
        vb = (1 + (self.mp/self.mb))*np.sqrt(2*self.g*self.l*(1 - np.cos(theta)))
        return np.round(vb,4)
    
    def grafPend(self): # Gráfica de oscilación del péndulo después del choque.
        t = np.arange(0, 10, 0.01) # Arreglo de tiempo.
        theta = np.radians(self.DesviacionAngulo())
        h = self.l*(1 - np.cos(theta*np.sin((np.sqrt(self.g/self.l))*t)))
        
        plt.figure(figsize=(10,8))
        plt.plot(t, h)
        plt.title('Oscilación del péndulo')
        plt.xlabel("Tiempo [s]")
        plt.ylabel("Altura Y [m]")
        plt.grid()
        plt.savefig('OscPendBalist.png')

# Parte de la herencia y polimorfismo.
class VelMinPendVuelt(PenduloBalistico):    
    def __init__(self, mb, mp, l, vp): # Método constructor.
        print('Inicializando clase VelMinPendVuelt.')
        
        super().__init__(mb, mp, l, vp) # Atributos de clase anterior.
        self.vp = np.sqrt(5*self.g*self.l) # Velocidad mínima péndulo. Polimorfismo.
    
    def velMinBala(self): # Velocidad mínima de bala para que péndulo dé una vuelta.
        vb = ((self.mb + self.mp)/self.mb)*self.vp
        return np.round(vb,4)
    
    # Polimorfismo suponiendo que choque sea elástico, i.e. no queden pegadas masas.
    def VelocidadBala(self): # Polimorfismo de método de clase anterior.
        vb = (self.mb + self.mp)/(2*self.mb)
        return print('La velocidad de la bala en un choque elástico es: {} m/s'.format(np.round(vb,4)))