import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

#Una clase para solucionar el pendulo balistico en función de las masas
class Pendulo_B:
    def __init__(self,masa_bala,masa_bloque,longitud,v0,g,dt, tmax,equation,theta0):
        self.masa_bala=masa_bala
        self.masa_bloque=masa_bloque
        self.longitud=longitud
        self.v0=v0
        self.g=g
    #Tiempos para la gráfica y ecuación
        self.dt=dt
        self.tmax=tmax
        self.t = np.arange(0, tmax, dt)
        self.equation=equation
        self.theta0=theta0
    #Método para la velocidad de las masas juntas
    def velocidad(self):
        self.v=self.v0*self.masa_bala/(self.masa_bala+self.masa_bloque)
        return self.v
    

    #Método para hallar el angulo desplaazado
    def angulo(self):
        self.theta=round(np.arcsin(1-((self.v0)**2 *(self.masa_bala)**2/2*(self.g*self.longitud*(self.masa_bala+self.masa_bloque)**2))),3)*180/np.pi
        return self.theta*np.pi/180
    
    #Una subclase para hallar la velocidad minima de la bala para una vuelta completa
class Velocidad_minima(Pendulo_B):
    def __init__(self,masa_bala,masa_bloque,longitud,v0,g,dt, tmax,equation,theta0,theta):
        self.theta=theta*np.pi/180
        super().__init__(masa_bala,masa_bloque,longitud,v0,g,dt, tmax,equation,theta0)
    #Para un angulo mayor a 90° 
    def velocidad_min(self):
        self.v_min=2*(self.masa_bala+self.masa_bloque)*round(np.sin(self.theta),3)*np.sqrt(self.g*self.longitud)/(self.masa_bala)
        return self.v_min
    #Calculemos la velocidad mínima de la bala unida al bloque para que el bloque haga una vuelta completa
    def velocidad_min2(self):
        self.v_min2=(self.masa_bala*self.velocidad_min())/(self.masa_bala+self.masa_bloque)
        return self.v_min2




#Clase para graficar la solución analítica 
class sol_gen:
    def __init__(self,h,xmax):
    #Inicialización de las variables
        self.h=h
        self.xmax=xmax
        self.x=np.arange(0,xmax,h)
        self.y=np.zeros(len(self.x))
         
    
    # Solución analítica para el oscilador armónico

    def graficar(self):
        plt.figure(figsize=(16,9))
        plt.title("Solución analítica pendulo balístico",fontsize=20)
        plt.plot(self.x, np.sin(self.x),color="black",label="Solución analítica")
        plt.xlabel("x",fontsize=20)
        plt.ylabel("y",fontsize=20)
        plt.legend(fontsize=20)
        plt.savefig("Grafica_test.png")
        plt.grid()
        plt.show()

